import { Given, Then, When, And } from "cypress-cucumber-preprocessor/steps";

import { transaction } from '../page_object/DecidMp';
const transactionModule = new transaction();

Given("i am on the transaction on the transaction page", () => {
  transactionModule.transactionButton().click();

});
When ("i see the topwallet button", () => {
    transactionModule.transactionButton().click();
    transactionModule.topwalletButton().should('be.visible');
  });
And ("i click on the topwallet button", () => {
  transactionModule.topwalletButton().click();

});
And ("i click on the amount field and type amount", () => {
  transactionModule.enteramountField().type('100');

});
And ("i click on the next button", () => {
  transactionModule.nextButton().click();

});
And ("i should see the amountName", () => {
  transactionModule.AmountName().should('be.visible');

});
Then ("i should the the page containing the paymentgatewayCharges", () => {
  transactionModule.paymentgatewayCharges().should('be.visible');
})
