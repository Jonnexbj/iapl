@transaction
Feature: transaction page on decide

i want to filter transactions 
Background: 
Given i am on the transaction page

Scenario: Filter transaction by status
   When i select the status button
   And i should see completed, waiting and canceled
   Then when i select the completed status i should see all completed status 

Scenario: Filter transaction by date
   When i input on preferred start date 
   And i input on preferred end date
   And i click on filterdate button
   # Then i should see table filtered by prefereed start and end date 
