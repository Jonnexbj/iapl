import { Given, And, Then, When } from "cypress-cucumber-preprocessor/steps";

import { transaction } from '../page_object/DecidMp';
const transactionModule = new transaction();

Given ("i am on the transaction page", () => {
    transactionModule.transactionButton().first().click();

  });
When ("i select the status button", () => {
    transactionModule.filterButton().click();
    transactionModule.statusButton().should('be.visible');
})
And ("i should see completed, waiting and canceled", () => {
    transactionModule.statusButton().select('Waiting').should('be.visible');
    transactionModule.statusButton().select('Canceled').should('be.visible');
    transactionModule.statusButton().select('Complete').should('be.visible');
});
Then("when i select the completed status i should see all completed status", () => {
    transactionModule.Completed().should('be.visible');      
});
//filter by date scenario

When ("i input on preferred start date", () => {
    transactionModule.startDate().click().type('2022-05-13');
})
And ("i input on preferred end date", () => {
    transactionModule.endDate().click().type('2022-05-16');
})
And ("i click on filterdate button", () => {
    transactionModule.filterDate().click();
})


