export class login {
    emailField() {
      return cy.get('#loginId');
    }
    passwordField() {
      return cy.get('#password');
    }
    loginButton() {
      return cy.get('.submit');
    }
  }
  export class setting {
    settingsButton() {
      return cy.get(':nth-child(4) > [data-testid="menu-item-href"]');
    }
    currentpasswordField() {
      return cy.get('[data-testid="current-password-field"]');
    }
    newpasswordField() {
      return cy.get('[data-testid="settings-Newpassword-field"]');
    }
    confirmpasswordField() {
      return cy.get('[data-testid="settings-confirmNewPassword-field"]');
    }
    resetButton() {
      return cy.get('.css-e5gaxo');
    }
    wrongcurrentPassword() {
      return cy.contains('Current password is incorrect');
    }
    passwordSuccess() {
      return cy.should('be.visible');
    }
  }
  export class transaction {
    transactionButton() {
      return cy.get('.css-0 > :nth-child(2)');
    }
    filterButton() {
      return cy.contains('Filter');
    }
    customerIdTxt() {
      return cy.get('thead.css-0 > .css-0 > :nth-child(1)');
    }
    datetxt() {
      return cy.get('thead.css-0 > .css-0 > :nth-child(2)');
    }
    amountTxt() {
      return cy.contains('AMOUNT');
    }
    statusTxt() {
      return cy.get('thead.css-0 > .css-0 > :nth-child(4)');
    }
    typeTxt() {
      return cy.contains('TYPE');
    }
    topwalletButton() {
      return cy.get('[data-testid="topup-wallet-button"] > .chakra-text');
    }
    enteramountField() {
      return cy.get('#field-4');
    }
    AmountButton() {
      return cy.get('#field-4-label');
    }
    nextButton() {
      return cy.get('[data-testid="upload-page-pagination?.next_page_url-btn"]');
    }
    nextButton2() {
      return cy.get('[data-testid="upload-page-next-btn"]');
    }
    backButton() {
      return cy.get('.css-x62lkx');
    }
    cancelButton() {
      return cy.get('.css-x62lkx');
    }
    statusButton() {
      return cy.get('[data-testid="status-dropdown"]');
    }
    filterDate() {
      return cy.get('[data-testid="filter-by-date-button"]');
    }
    completeTransaction() {
      return cy.get('[data-testid=upload-page-next-btn]');
    }
    Completed() {
      return cy.contains('Completed');
    }
    startDate() {
      return cy.get('[data-testid="set-startdate"]');
    }
    endDate() {
      return cy.get('[data-testid="set-endtdate"]');
    }
    AmountName() {
      return cy.get('tbody.css-0 > :nth-child(1) > :nth-child(1)');
    }
    paymentgatewayCharges() {
      return cy.get('tbody.css-0 > :nth-child(2) > :nth-child(1)');
    }
    total() {
      return cy.get('tbody.css-0 > :nth-child(3) > :nth-child(1)');
    }
  }
  